@extends('pure_message')

@section('content')
    <div class="message-box">
        <h1>Error</h1>

        <hr>

        <p>Sorry, something bad happened. Our website has crashed.</p>
        <p>Don't worry, we will handle this. In the meantime, make a cup of coffee. And stop crushing our website!</p>


        <hr>

        <p> Lo siento, algo malo pasó. Nuestro sitio web se ha estrellado. </p>
        <p> No se preocupe, vamos a manejar esto. Mientras tanto, haz una taza de café. ¡Y deja de aplastar nuestro sitio web! </p>


        <hr>
        
        <button onclick="javascript:window.location='{!! route('home') !!}'">Website</button>
    </div>
@stop