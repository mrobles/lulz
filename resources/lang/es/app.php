<?php 

return [

    /*
    |--------------------------------------------------------------------------
    | Global Language Lines
    |--------------------------------------------------------------------------
    |
    | CMS global language lines
    |
    */
   
    /*
     * Date format patterns
     */
    'date_format'        => 'd-m-Y',         // PHP default, for Carbon ( http://www.php.net/manual/en/function.date.php )
    'date_format_alt'    => 'DD-MM-YYYY',    // for the date time picker UI widget
   
    /*
     * Security
     */
    'spam_protection' => 'Para protección contra spam, tienes que esperar unos segundos hasta que puedas enviar este formulario.',
    'debug_warning'   => 'Advertencia: El modo de depuración está habilitado pero esto no parece ser un servidor de desarrollo!',

    /*
     * Errors
     */
    'not_found'     => 'Recurso no encontrado.',
    'not_possible'  => 'Lo siento, pero esta acción no está disponible ahora.',

    /*
     * User permisssions
     */ 
    'no_auth'       => 'Acceso denegado. Por favor Iniciar sesión.',
    'access_denied' => 'Acceso denegado. Necesita permisos.',
    
    /*
     * Forms
     */
    'save'      => 'Guardar',
    'apply'     => 'Aplicar',
    'reset'     => 'Restablecer',
    'update'    => 'Actualizar',

    /*
     * Auto CRUD handling (and related stuff)
     */
    'created'       => ':0 creado.',
    'updated'       => ':0 actualizado.',
    'deleted'       => ':0 eliminado.',
    'restored'      => ':0 restaurado.',
    'list_empty'    => 'Por favor, crea al menos uno ":0" antes de continuar.',
    'invalid_image' => 'Archivo de imagen no válido',
    'bad_extension' => "La carga de archivos con la extensión ':0' está restringida.",
    'delete_error'  => "Error: Este objeto tiene :0 dependencias (ID: ':1')! Por favor, eliminelas antes.",

    /*
     * Model index building
     */
    'create'        => 'Crear nuevo',
    'categories'    => 'Categorías',
    'config'        => 'Configuración',
    'actions'       => 'Acciones',
    'edit'          => 'Editar',
    'delete'        => 'Eliminar',
    'restore'       => 'Restaurar',
    
    'id'            => 'ID',
    'index'         => 'Índice',
    'title'         => 'Título',
    'short_title'   => 'Título corto',
    'author'        => 'Autor',
    'creator'       => 'Creador',
    'category'      => 'Categoría',
    'type'          => 'Tipo',
    'provider'      => 'Proveedor',
    'text'          => 'Texto',
    'code'          => 'Código',
    'description'   => 'Descripción',
    'image'         => 'Imagen',
    'icon'          => 'Icono',
    'date'          => 'Fecha',
    'published'     => 'Publicado',
    'starts_at'     => 'Inicia en',
    'created_at'    => 'Inicia en',
    'updated_at'    => 'Actualizado en',
    'achieved_at'   => 'Logrado en',
    'internal'      => 'Interno',
    'featured'      => 'Destacados',
    'state'         => 'Estado',
    'new'           => 'Nuevo',

    /*
     * Misc
     */
    'sorting'       => 'Ordenar',
    'search'        => 'Buscar',
    'recycle_bin'   => 'Papelera de reciclaje',
    'max_size'      => 'Máx. Tamaño: :0Bytes',
    'accessed'      => 'Accedido',
    'no_items'      => 'No hay elementos.',
    'share_this'    => 'Compartir esto',
    'save_to_del'   => 'Guardar este formulario para eliminar este archivo.',

    /*
     * JS
     */
    'request_failed'    => 'Error: Error de la solicitud.',
    'delete_item'       => '¿Eliminar este elemento?',
    'perform_action'    => '¿Ejecutar esta acción?',

    /*
     * Backend template
     */
    'no_js'             => 'JavaScript no habilitado. Por favor, activa JavaScript!',
    'admin_dashboard'   => 'Admin-Dashboard',
    'new_messages'      => 'Un nuevo mensaje|:count nuevos mensajes',
    'top'               => 'Ir al principio',
    'help'              => 'Ayuda',
    'welcome'           => 'Bienvenido',


    /*
     * Comments
     */
    'comments'              => 'Comentarios',
    'comment_create_fail'   => 'No se pudo crear comentario. :0',
    'comment_update_fail'   => 'No se pudo actualizar comentario. :0',
    'enable_comments'       => 'Activar comentarios',

    /*
     * Captcha
     */
    'captcha_invalid'   => '¡El código captcha no es válido!',

    /*
     * General terms
     */
    'date_time'         => 'Fecha y hora',
    'yes'               => 'Sí',
    'no'                => 'No',
    'valid'             => 'válido',
    'invalid'           => 'inválido',
    'enabled'           => 'Habilitado',
    'disabled'          => 'Desactivado',
    'read_more'         => 'Leer más ...',
    'name'              => 'Nombre',
    'username'          => 'Nombre de usuario',
    'email'             => 'Correo electrónico',
    'password'          => 'Contraseña',
    'file'              => 'Archivo',
    'link'              => 'Enlace',
    'links'             => 'Enlaces',
    'send'              => 'Enviar',
    'position'          => 'Posición',
    'location'          => 'Ubicación',
    'url'               => 'URL',
    'quote'             => 'Cita',
    'size'              => 'Tamaño',
    'ip'                => 'IP',
    'online'            => 'En línea',
    'offline'           => 'Desconectado',
    'viewers'           => 'Espectadores',
    'task'              => 'Tarea',
    'add_tags'          => 'Añadir etiquetas',
    'reply'             => 'Responder',
    'latest'            => 'Últimos',
    'item'              => 'Elemento',
    'home'              => 'Inicio',
    'remove'            => 'Eliminar',
    'of'                => 'de',
    'role'              => 'Rol',
    'message'           => 'Mensaje',
    'subject'           => 'Asunto',
    'latest_msgs'       => 'Últimos mensajes de feed', // Admin dashboard feed messages
    'quick_access'      => 'Acceso rápido', // Admin dashboard quick access
    'setting'           => 'Configuración',
    'value'             => 'Valor',
    'placeholder'       => 'Marcador de posición, por favor cambie!', // Diag module
    'compiled'          => 'compilado',
    'website'           => 'Sitio web',
    'calendar'          => 'Calendario',
    'team_members'      => 'Miembros del equipo',
    'profile'           => 'Perfil',
    'edit_profile'      => 'Editar perfil',
    'add'               => 'Añadir',
    'logout'            => 'Cerrar sesión',
    'install'           => 'Instalar',
    'preview'           => 'Vista previa',
    'total'             => 'Total',
    'nothing_new'       => 'Lo siento, nada nuevo. Por favor regrese más tarde.',
    'back'              => 'Atrás',
    'theme'             => 'Tema',
    'theme_christmas'   => 'Modo de Navidad (Nieve)',
    'theme_snow_color'  => 'Escamas de color',
    'loading'           => 'Cargando',
    'lineup'            => 'Alineación',
    'translate'         => 'Traducir',
    'successful'        => '¡Exito!',
    'slots'             => 'Ranuras',
    'mode'              => 'Modo',
    'prize'             => 'Premio',
    'closed'            => 'Cerrado',
    'leave'             => 'Dejar',
    'join'              => 'Unirse',
    'confirm'           => 'Confirmar',
    'rules'             => 'Reglas',
    'round'             => 'Ronda',


    /*
     * Days
     */
    'yeah'          => 'Año',
    'month'         => 'Mes',
    'day'           => 'Día',
    'today'         => 'Hoy',
    'yesterday'     => 'Ayer',
    'tomorrow'      => 'Mañana',
    'monday'        => 'Lunes',
    'tuesday'       => 'Martes',
    'wednesday'     => 'Miércoles',
    'thursday'      => 'Jueves',
    'friday'        => 'Viernes',
    'saturday'      => 'Sábado',
    'sunday'        => 'Domingo',


    /*
     * Module names
     */
    'object_adverts'        => 'Anuncios',
    'object_auth'           => 'Autenticación',
    'object_awards'         => 'Premios',
    'object_comments'       => 'Comentarios',
    'object_config'         => 'Configuración',
    'object_contact'        => 'Contacto',
    'object_countries'      => 'Países',
    'object_cups'           => 'Copas',
    'object_dashboard'      => 'Cuadro de instrumentos',
    'object_diag'           => 'Diagnóstico',
    'object_downloads'      => 'Descargas',
    'object_events'         => 'Eventos',
    'object_forums'         => 'Foros',
    'object_friends'        => 'Amigos',
    'object_galleries'      => 'Galerías',
    'object_games'          => 'Juegos',
    'object_images'         => 'Imágenes',
    'object_languages'      => 'Idiomas',
    'object_maps'           => 'Mapas',
    'object_matches'        => 'Partidas',
    'object_messages'       => 'Mensajes',
    'object_modules'        => 'Módulos',
    'object_navigations'    => 'Navegaciones',
    'object_news'           => 'Noticias',
    'object_opponents'      => 'Oponentes',
    'object_pages'          => 'Páginas',
    'object_partners'       => 'Partners',
    'object_roles'          => 'Roles',
    'object_search'         => 'Buscar',
    'object_servers'        => 'Servidores',
    'object_shouts'         => 'Gritos',
    'object_slides'         => 'Slides',
    'object_streams'        => 'Streams',
    'object_teams'          => 'Equipos',
    'object_tournaments'    => 'Torneos',
    'object_update'         => 'Actualizar',
    'object_users'          => 'Usuarios',
    'object_videos'         => 'Vídeos',
    'object_visitors'       => 'Visitantes',


    /*
     * Model names
     */
    'object_advert'             => 'Publicidad',
    'object_advertcat'          => 'Publicidad-Categoría',
    'object_award'              => 'Premio',
    'object_contact_message'    => 'Mensaje de contacto',
    'object_join_message'       => 'Aplicación',
    'object_country'            => 'País',
    'object_cup'                => 'Copa',
    'object_download'           => 'Descargar',
    'object_downloadcat'        => 'Descargar-Categoría',
    'object_event'              => 'Evento',
    'object_post'               => 'Post',
    'object_thread'             => 'Hilo',
    'object_forum'              => 'Foro',
    'object_forum_post'         => 'Mensaje del Foro',
    'object_forum_report'       => 'Informe del Foro',
    'object_forum_thread'       => 'Hilo del Foro',
    'object_gallery'            => 'Galería',
    'object_game'               => 'Juego',
    'object_image'              => 'Imagen',
    'object_language'           => 'Idioma',
    'object_map'                => 'Mapa',
    'object_match'              => 'Partido',
    'object_match_score'        => 'Resultado del partido',
    'object_messages'           => 'Mensaje',
    'object_module'             => 'Módulo',
    'object_navigation'         => 'Navegación',
    'object_newscat'            => 'Noticias-Categoría',
    'object_opponent'           => 'Oponente',
    'object_article'            => 'Artículo',
    'object_custom_page'        => 'Página personalizada',
    'object_fragment'           => 'Fragmento',
    'object_page'               => 'Página',
    'object_pagecat'            => 'Página-Category',
    'object_partner'            => 'Partner',
    'object_partnercat'         => 'Partner-Categoría',
    'object_role'               => 'Rolle',
    'object_server'             => 'Servidor',
    'object_slide'              => 'Diapositiva',
    'object_slidecat'           => 'Diapositiva-Categoría',
    'object_stream'             => 'Stream',
    'object_team'               => 'Equipo',
    'object_teamcat'            => 'Equipo-Categoría',
    'object_tournament'         => 'Torneo',
    'object_user'               => 'Usuario',
    'object_video'              => 'Video',

    /* 
     * Controller names (without "admin" prefix)
     */
    'object_advertcats'         => 'Categorías-Publicidad',
    'object_login'              => 'Iniciar sesión',
    'object_registration'       => 'Registro',
    'object_restore_password'   => 'Restaurar contraseña',
    'object_join'               => 'Unirse',
    'object_reports'            => 'Informes',
    'object_forum_reports'      => 'Foro-Informes',
    'object_inbox'              => 'Bandeja de entrada',
    'object_outbox'             => 'Bandeja de salida',
    'object_newscats'           => 'Categorías de noticias',
    'object_articles'           => 'Artículos',
    'object_custom_pages'       => 'Páginas personalizadas',
    'object_partnercats'        => 'Categorías de socios',
    'object_slidecats'          => 'Categorías de diapositivas',
    'object_members'            => 'Miembros',
    'object_activities'         => 'Actividades',
    'object_threads'            => 'Hilos',
    'object_match_scores'       => 'Resultado del partido',
    'object_participants'       => 'Participantes',


];
