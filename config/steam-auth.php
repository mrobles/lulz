<?php

return [

    /*
     * Redirect URL after login
     */
    'redirect_url' => '/auth/steam',
    /*
     * Enter your API Key (http://steamcommunity.com/dev/apikey) here:
     */
    'api_key' => 'C15BE3FD2BBC15C3451D7B25ECF46D82',
    /*
     * Is using https ?
     */
    'https' => false

];