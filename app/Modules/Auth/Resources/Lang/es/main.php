<?php 

return array(

    /*
    |--------------------------------------------------------------------------
    | Module Language Lines
    |--------------------------------------------------------------------------
    |
    | This file keeps the language lines of the related module.
    |
    */

    /*
     * Logout
     */
    'logged_out'        => '¡Deslogearse!',

    /*
     * Registration
     */
    'register'          => 'Registrarse',
    'registered'        => '¡Registro con éxito!',
    'steam_registered'  => '¡Registro a través de Steam con éxito! Si también quieres habilitar el inicio de sesión por correo electrónico, configura tu dirección de correo electrónico y solicita una nueva contraseña.',
    'username_taken'    => 'Nombre de usuario ya utilizado.',
    'password_length'   => 'Tu contraseña debe tener al menos 6 caracteres.',
    'register_twice'    => 'Ya está registrado.',



    /*
     * Restore password
     */
    'password_reset'    => 'Restablecer contraseña',
    'email_gen_pw'      => 'Se ha enviado un correo electrónico a la dirección de correo electrónico especificada. Siga las instrucciones para generar una nueva contraseña.',
    'email_invalid'     => 'No se encontró usuario con la dirección de correo electrónico especificada.',
    'code_invalid'      => 'El código no coincide.',
    'new_pw'            => 'Nueva contraseña',
    'email_new_pw'      => 'Se ha enviado un correo electrónico con su nueva contraseña a su dirección de correo electrónico.',
    'pw_link'           => 'Por favor, haga clic en el siguiente enlace para generar una nueva contraseña para :0.',
    'email_ignore'      => 'Si no desea generar una nueva contraseña, por favor ignore este correo.',
    'pw_generated'      => 'Se ha generado una nueva contraseña para :0.',

    /*
     * Login 
     */
    'login'             => 'Logearse',

    /*
     * Widget
     */
    'backend'           => 'Admin-Backend',

);