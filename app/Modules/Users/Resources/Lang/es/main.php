<?php 

return array(

    /*
    |--------------------------------------------------------------------------
    | Module Language Lines
    |--------------------------------------------------------------------------
    |
    | This file keeps the language lines of the related module.
    |
    */
   
    /*
     * Profile
     */
    'first_name'    => 'Nombre',
    'last_name'     => 'Apellidos',
    'gender'        => 'Sexo',
    'localisation'  => 'Localización',
    'local_info'    => 'Establece el idioma de la interfaz y el formato de fecha.',
    'birthdate'     => 'Cumpleaños',
    'occupation'    => 'Ocupación',

    'about'         => 'Acerca de',
    'signature'     => 'Firma',

    'steam_id'      => 'Steam ID',

    'cpu'           => 'CPU',
    'graphics'      => 'Gráfica',
    'ram'           => 'RAM',
    'motherboard'   => 'Placa Base',
    'drives'        => 'Drives',
    'display'       => 'Pantalla',
    'mouse'         => 'Ratón',
    'keyboard'      => 'Teclado',
    'headset'       => 'Auriculares',
    'mousepad'      => 'Alfombrilla',

    'food'          => 'Comida',
    'drink'         => 'Bebida',
    'music'         => 'Musica',
    'film'          => 'Peliculas',

    'image'         => 'Perfil',
    'avatar'        => 'Avatar',

    'send_msg'      => 'Enviar Mensage',
    'add_friend'    => 'Agregar a Amigos',

    /*
     * Gender
     */
    'unknown'       => 'Desconocido',
    'female'        => 'Femenino',
    'male'          => 'Masculino',
    'other'         => 'Otro',

    'change'        => 'Cambio...',

    /*
     * Change password
     */
    'change_pw'     => 'Cambiar contraseña',
    'current_pw'    => 'Contraseña actual',
    'new_pw'        => 'Nueva contraseña',

    /*
     * User list
     */
    'last_login'    => 'Ultimo inicio de sesión',

    /*
     * Backend
     */
    'membership'    => 'Membresía',
    'banned'        => 'Baneado',
    'activated'     => 'Activado',

    'action_activated'     => 'Se ha activado el usuario.',
    'action_deactivated'   => 'El usuario ha sido desactivado.',

    /* 
     * Widget
     */
    'latest_users'  => 'Últimos usuarios',

    /*
     * Activities
     */
    'frontend'      => 'Frontend', 
    'model_class'   => 'Modelo',
    'activity'      => 'Actividad',



);