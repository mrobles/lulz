<?php 

return array(

    /*
    |--------------------------------------------------------------------------
    | Module Language Lines
    |--------------------------------------------------------------------------
    |
    | This file keeps the language lines of the related module.
    |
    */
   
    'written_by'         => 'Compentario escrito por',
    'written_on'         => 'en', // Date
    'written_at'         => 'a las', // Time
    'write'              => 'Escribir un comentario:',
    'login_info'         => 'Para comentar, :0 o :1.',

    /* 
     * Widget
     */
    'latest_comments'   => 'Ultimos comentarios',

);