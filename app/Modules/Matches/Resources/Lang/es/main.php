<?php 

return array(

    /*
    |--------------------------------------------------------------------------
    | Module Language Lines
    |--------------------------------------------------------------------------
    |
    | This file keeps the language lines of the related module.
    |
    */
   
    'broadcast'     => 'Difusión',
    'left_team'     => 'Equipo Local',
    'right_team'    => 'Equipo Visitante',
    'left_lineup'   => 'Alineación Equipo Local',
    'right_lineup'  => 'Alineación Equipo Visitante',
    'played_at'     => 'Jugado en',
    'score'         => 'Resultado',
    'vs'            => 'vs',


);