<?php 

return array(

    /*
    |--------------------------------------------------------------------------
    | Module Language Lines
    |--------------------------------------------------------------------------
    |
    | This file keeps the language lines of the related module.
    |
    */
   
    'summary'       => 'Resumen',
    'written_by'    => 'escrito por',
    'in'            => 'en', // in category
    'publish_at'    => 'Publicado en', // in category
    'rss_last'      => 'Ultimas 20 notícias',

);