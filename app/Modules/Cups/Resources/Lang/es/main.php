<?php 

return array(

    /*
    |--------------------------------------------------------------------------
    | Module Language Lines
    |--------------------------------------------------------------------------
    |
    | This file keeps the language lines of the related module.
    |
    */
    
    // Cup
    'join_at'           => 'Unirse a',
    'check_in_at'       => 'Registrarse en',
    'start_at'          => 'Iniciar en',
    'checked_in'        => 'Checked In',
    'seeding'           => 'Seeding',
    'players_per_team'  => 'Jugadores por equipo',
    'bracket'           => 'Bracket',
    'cup_full'          => 'Máximo número de participantes alcanzados.',
    'cannot_join'       => 'El registro de copa no está abierto todavía - espere hasta que comience la fase de unión.',
    'joined'            => 'Estás participando. Por favor espere la fase de check-in.',
    'join_hint'         => 'Haz clic aquí para unirte a la copa:',
    'join'              => 'Únete ahora',
    'no_team'           => '¡No tienes equipo para unirte a la copa!',
    'create_team'       => 'Crear equipo',
    'check_out'         => 'Haga clic aquí para salir:',
    'check_in'          => 'Haga clic aquí para registrarse:',
    'not_participating' => 'No estás participando en esta copa.',
    'cup_running'       => 'La copa está en marcha. Por favor, revise los Brackets para ver los partidos.',
    'cup_closed'        => 'La copa ha sido cerrada.',
    'login_hint'        => 'Ingresa para obtener acceso a esta copa!',
    'referees'          => 'Árbitros',
    'swap'              => 'Intercambiar',
    'in_no_cups'        => 'No estás registrado para ninguna copa.',
    'join_a_cup'        => '¡Únete a una copa ahora!',



    // Team
    'not_organizer'     => 'Sólo un miembro del equipo con permisos de organizador está autorizado a hacerlo.',
    'organizer'         => 'Organizador',
    'user_conflict'     => 'Un usuario no puede estar en más de un equipo por copa.',
    'team_locked'       => 'El equipo está bloqueado mientras participa en una copa. No se pueden hacer cambios.',
    'edit_team'         => 'Editar equipo',
    'team_deleted'      => 'Este equipo ha sido eliminado.',
    'my_teams'          => 'Mis equipos',
    'wrong_password'    => '¡Contraseña incorrecta! Por favor, inténtelo de nuevo.',
    'min_organizers'    => 'Esto no es posible. El equipo debe tener al menos un organizador.',

    // Match
    'confirm_score'     => 'Confirmar resultado',
    'new_match'         => '¡Se ha generado un nuevo partido!',
    'change_winner'     => 'Cambiar ganador',
    'next_match'        => 'Siguiente partido',


);